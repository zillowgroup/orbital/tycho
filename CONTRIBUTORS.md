# Contributors

## Pre-OSS

During the open source process, the history of Tycho was started anew.

Thanks to all of those who contributed before that point:

* Yusuke Tsutsumi
* Saroj Kandel
* Deepika Ahirrao
* Yun Xu
* Mayur Mahajan
* Josh Green
* Nandkishor Deore
* Shweta Sharma
* Brendan Almonte
* Sean Carberry
* Tianyi Chang
